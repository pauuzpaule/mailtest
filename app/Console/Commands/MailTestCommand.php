<?php

namespace App\Console\Commands;

use App\Lib\MailSender;
use Illuminate\Console\Command;

class MailTestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return $this->sendMail();
    }

    public function sendMail()
    {
        /*transport = (new Swift_SmtpTransport('smtp.gmail.com', 465, 'ssl'))
            ->setUsername('dev.borrowercentral@gmail.com')
            ->setPassword('@borrower#123');*/

        $mail = new MailSender("app@sayrunjah.com", "Test Mail");
        try {
            $res = $mail->to(["pausepaule@gmail.com"])->subject("Verification Code")->send("Hello .....");
            dd($res);
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }

    }
}
