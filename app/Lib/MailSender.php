<?php


namespace App\Lib;


use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class MailSender
{
    public $to;
    public $from;
    private $mail;
    public $subject;

    public function  __construct($from, $name ="")
    {
        $this->mail = new PHPMailer(true);
        $this->mail->setFrom("pausepaulo@gmail.com", $name);

        //config
       $this->mail->isSMTP(); // Set mailer to use SMTP
        $this->mail->Host = "email-smtp.us-east-2.amazonaws.com";
        $this->mail->Port = 465;
        $this->mail->SMTPSecure = "ssl";
        $this->mail->SMTPAutoTLS = false;
        $this->mail->SMTPAuth = true;
        $this->mail->Username   = "AKIAQUUC6U727XVQP6GV";                     // SMTP username
        $this->mail->Password   = "BEzBTrs+OuotMaSO6vKALmxcPLfB4jiIcj2cO+piGcUz";

        //$this->mail->Username   = "dev.borrowercentral@gmail.com";                     // SMTP username
        //$this->mail->Password   = "@borrower#123";

        /*if(env("MAIL_ENV") == "test") {
            $this->mail->isSMTP(); // Set mailer to use SMTP
            $this->mail->Host = env('MAIL_HOST');
            $this->mail->Port = env('MAIL_PORT');
            $this->mail->SMTPSecure = false;
            $this->mail->SMTPAutoTLS = false;
            $this->mail->SMTPAuth = true;
            $this->mail->Username   = env('MAIL_USERNAME');                     // SMTP username
            $this->mail->Password   = env('MAIL_PASSWORD');
        }else {
            $this->mail->isSMTP(); // Set mailer to use SMTP
            $this->mail->Host = 'localhost';
            $this->mail->Port = 25;
            $this->mail->SMTPSecure = false;
            $this->mail->SMTPAutoTLS = false;
            $this->mail->SMTPAuth = false;
        }*/



        $this->mail->isHTML(true);
    }

    public function subject($subject)
    {
        $this->mail->Subject = $subject;
        return $this;
    }

    public function to(array $emails)
    {
        foreach($emails as $email) {
            try {
                $this->mail->addAddress($email);
            } catch (Exception $e) {

            }
        }
        return $this;
    }

    public function send($body)
    {

        try {
            $this->mail->Body = $body;
            $this->mail->send();
            return "SENT";
        } catch (Exception $e) {
            return $e;
            // return "Message could not be sent. Mailer Error: {$this->mail->ErrorInfo} Other Error: ".$e->getMessage();
        }
    }
}
